
https://hub.docker.com/_/node

https://github.com/nodejs/docker-node/blob/master/README.md#how-to-use-this-image

https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md

```
docker-compose up -d
```

```
docker run \
  --rm \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node [script]
```
