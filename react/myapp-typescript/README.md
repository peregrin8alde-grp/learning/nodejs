# React + Typescript

参考：

- https://ja.reactjs.org/docs/static-type-checking.html#typescript
- https://create-react-app.dev/docs/adding-typescript
- https://www.typescriptlang.org/docs/home.html
- https://qiita.com/HiroshiAkutsu/items/57de9bf817b656ec62c6

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app myapp-typescript --template typescript

cd myapp-typescript

docker run \
  --rm \
  -it \
  --name myapp-typescript \
  -p 3000:3000 \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    yarn start
```

```
docker run \
  --rm \
  -it \
  --name myapp-typescript \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    yarn build
```
