import React from "react";
import Item1 from "./components/Item1";

function App() {
  return (
    <div className="App">
      <Item1
        message="msg"
        count={2}
        disabled={false}
        names={["111", "222"]}
        status="waiting"
        obj3={{ id: "aa", title: "bb" }}
        objArr={[
          {
            id: "cc",
            title: "dd",
          },
          {
            id: "ee",
            title: "ff",
          },
        ]}
        onChange={(id: number): void => {
          console.log("onChange");
          console.log(id);
        }}
        onClick={(e: React.MouseEvent<HTMLButtonElement>): void => {
          console.log("onClick");
          console.log(e);
          console.log(e.target);
          console.log(e.currentTarget);
        }}
      />
    </div>
  );
}

export default App;
