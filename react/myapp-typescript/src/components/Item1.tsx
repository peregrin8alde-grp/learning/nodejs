import React from "react";
import Form1 from "./Form1";

type OptionalType = {
  option1: boolean;
};

type AppProps = {
  message: string;
  count: number;
  disabled: boolean;
  /** array of a type! */
  names: string[];
  /** string literals to specify exact string values, with a union type to join them together */
  status: "waiting" | "success";
  /** any object as long as you dont use its properties (not common) */
  //obj: object;
  //obj2: {}; // almost the same as `object`, exactly the same as `Object`
  /** an object with defined properties (preferred) */
  obj3: {
    id: string;
    title: string;
  };
  /** array of objects! (common) */
  objArr: {
    id: string;
    title: string;
  }[];
  /** any function as long as you don't invoke it (not recommended) */
  //onSomething: Function;
  /** function that doesn't take or return anything (VERY COMMON) */
  //onClick: () => void;
  /** function with named prop (VERY COMMON) */
  onChange: (id: number) => void;
  /** alternative function type syntax that takes an event (VERY COMMON) */
  onClick(event: React.MouseEvent<HTMLButtonElement>): void;
  /** an optional prop (VERY COMMON!) */
  optional?: OptionalType;
};

type MyState = {
  count: number; // like this
};

class Item1 extends React.Component<AppProps, MyState> {
  state: MyState = {
    // optional second annotation for better type inference
    count: this.props.count,
  };

  onChange1 = () => {
    console.log("onChange1");
    this.props.onChange(100);
  };

  render() {
    return (
      <div>
        <form onChange={this.onChange1}>
          <label>{this.state.count}</label>
          <label>{this.props.message}</label>
          <label>{this.props.count}</label>
          <input
            type="checkbox"
            checked={this.props.disabled}
            onChange={this.onChange1}
          />
          <label>{this.props.names}</label>
          <label>{this.props.status}</label>
          <label>{this.props.obj3.id}</label>
          <label>{this.props.obj3.title}</label>
          <label>{this.props.objArr[0].id}</label>
          <label>{this.props.objArr[0].title}</label>
          <button name="btn1" onClick={this.props.onClick}>
            btn1
          </button>
        </form>

        <Form1 />
      </div>
    );
  }
}

export default Item1;
