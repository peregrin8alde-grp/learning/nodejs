import React from "react";

type Form1Props = {};

type Form1State = {
  text: string;
};

class Form1 extends React.Component<Form1Props, Form1State> {
  state: Form1State = {
    text: "",
  };

  // typing on RIGHT hand side of =
  onChange = (e: React.FormEvent<HTMLInputElement>): void => {
    console.log("onChange");
    console.log(e);
    console.log(e.target);
    console.log(e.currentTarget);
    this.setState({ text: e.currentTarget.value });
  };

  onChangeForm = (e: React.FormEvent<HTMLFormElement>): void => {
    console.log("onChangeForm");
    console.log(e);
    console.log(e.target);
    console.log(e.currentTarget);
  };

  onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();

    console.log("onSubmit");
    console.log(e);
    console.log(e.target);
    console.log(e.currentTarget);

    const target = e.target as typeof e.target & {
      email: { value: string };
      password: { value: string };
    };
    const email = target.email.value; // typechecks!
    const password = target.password.value; // typechecks!

    console.log(email);
    console.log(password);
  };

  render() {
    return (
      <div>
        <input
          type="text"
          id="text"
          value={this.state.text}
          onChange={this.onChange}
        />

        <form onChange={this.onChangeForm} onSubmit={this.onSubmit}>
          <div>
            <label>
              Email:
              <input type="email" id="email" name="email" />
            </label>
          </div>
          <div>
            <label>
              Password:
              <input type="password" id="password" name="password" />
            </label>
          </div>
          <div>
            <input type="submit" value="Log in" />
          </div>
        </form>
      </div>
    );
  }
}

export default Form1;
