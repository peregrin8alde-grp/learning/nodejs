# Tutorial

https://ja.reactjs.org/tutorial/tutorial.html

## 新規作成

```
docker run \
  --rm \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node npx create-react-app tutorial
```

デフォルトではjs/cssの参照が絶対パスとなるため、相対パスにするため `package.json` で `"homepage": "./"` 指定を行う。


