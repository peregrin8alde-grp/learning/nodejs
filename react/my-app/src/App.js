import React from 'react';
import './App.css';

import SubmitForm from './components/SubmitForm';

class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: this.props.todo.task,
      isDone: false
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      task: event.target.value
    });
  }

  render() {
    return (
      <form>
        <label>
          {this.props.todo.uuid}:
          <input type="text" value={this.state.task} onChange={this.handleChange} />
        </label>
      </form>
    );
  }
}

class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };

    this.handleAdd = this.handleAdd.bind(this);
  }

  componentDidMount() {
    // 同一ドメインじゃないとCORSが発生する。開発時のみ別ドメインなどの場合は proxy 機能を利用
    // https://create-react-app.dev/docs/proxying-api-requests-in-development/
    fetch("/api/data")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  handleAdd(event) {
    fetch("/api/data", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ task: event.target.task.value, isDone: false })
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);

          // ここでデータを全部再取得するか、クライアント側で追加分だけ変更するようなリストのstate変更
          // どっちが普通？
          const items = this.state.items.slice();
          this.setState({ items: items.concat([result]) });
        },
        // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
        // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
        (error) => {
          this.setState({
            error
          });
        }
      )

    event.target.task.value = '';
    event.preventDefault();
  }

  render() {
    const { error, isLoaded, items } = this.state;

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      const listItems = items.map((item) =>
        <li key={item.uuid}>
          <Todo todo={item}></Todo>
        </li>
      );

      return (
        <div>
          <ul>{listItems}</ul>
          <SubmitForm handleAdd={this.handleAdd} />
        </div>
      );
    }
  }
}

function App() {
  return (
    <TodoList />
  );
}

export default App;
