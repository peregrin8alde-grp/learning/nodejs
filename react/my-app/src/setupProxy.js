const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/api/',
    createProxyMiddleware({
      target: 'http://nodered:1880',
      changeOrigin: true,
    })
  );
  app.use(
    '/api/list2/*',
    createProxyMiddleware({
      target: 'http://nodered:1880',
      changeOrigin: true,
    })
  );
};
