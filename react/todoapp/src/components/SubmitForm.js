import React from 'react';

function SubmitForm(props) {
    return (
        <form onSubmit={props.handleAdd}>
            <label>
                <input name="task" type="text" />
            </label>
            <input type="submit" value="Add" />
        </form>
    );
}

export default SubmitForm;
