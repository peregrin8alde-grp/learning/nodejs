import React from 'react';

import SubmitForm from './SubmitForm';
import TodoItem from './TodoItem';

class TodoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };

        this.handleAdd = this.handleAdd.bind(this);
        this.handleDel = this.handleDel.bind(this);
        this.handlePut = this.handlePut.bind(this);
        this.handlePatch = this.handlePatch.bind(this);
    }

    componentDidMount() {
        // 同一ドメインじゃないとCORSが発生する。開発時のみ別ドメインなどの場合は proxy 機能を利用
        // https://create-react-app.dev/docs/proxying-api-requests-in-development/
        fetch("/api/data")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
                // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    handleAdd(event) {
        fetch("/api/data", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ task: event.target.task.value, isDone: false })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);

                    // ここでデータを全部再取得するか、クライアント側で追加分だけ変更するようなリストのstate変更
                    // どっちが普通？
                    const items = this.state.items.slice();
                    this.setState({ items: items.concat([result]) });
                },
                // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
                // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
                (error) => {
                    this.setState({
                        error
                    });
                }
            )

        event.target.task.value = '';
        event.preventDefault();
    }

    // 関数呼び出しで引数を使いたい場合
    // https://ja.reactjs.org/docs/faq-functions.html
    handlePut(uuid, event) {
        fetch("/api/data/" + uuid, {
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ task: event.target.task.value, isDone: event.target.isDone.checked })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);

                    // ここでデータを全部再取得するか、クライアント側で削除分だけ変更するようなリストのstate変更
                    // どっちが普通？
                    const UpdateItems = (arr, newItem) => arr.map(item => (item.uuid === newItem.uuid) ? newItem : item)
                    this.setState({ items: UpdateItems(this.state.items, result) });
                },
                // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
                // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
                (error) => {
                    this.setState({
                        error
                    });
                }
            )

        event.preventDefault();
    }

    handlePatch(uuid, event) {
        fetch("/api/data/" + uuid, {
            method: "PATCH",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ isDone: event.target.checked })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);

                    // ここでデータを全部再取得するか、クライアント側で削除分だけ変更するようなリストのstate変更
                    // どっちが普通？
                    const UpdateItems = (arr, newItem) => arr.map(item => (item.uuid === newItem.uuid) ? newItem : item)
                    this.setState({ items: UpdateItems(this.state.items, result) });
                },
                // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
                // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
                (error) => {
                    this.setState({
                        error
                    });
                }
            )

        event.preventDefault();
    }

    handleDel(uuid) {
        fetch("/api/data/" + uuid, {
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);

                    // ここでデータを全部再取得するか、クライアント側で削除分だけ変更するようなリストのstate変更
                    // どっちが普通？
                    const items = this.state.items.slice();
                    this.setState({ items: items.filter(item => item.uuid !== uuid) });
                },
                // 補足：コンポーネント内のバグによる例外を隠蔽しないためにも
                // catch()ブロックの代わりにここでエラーハンドリングすることが重要です
                (error) => {
                    this.setState({
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            const listItems = items.map((item) =>
                <li key={item.uuid}>
                    <TodoItem
                        todo={item}
                        handlePut={(e) => this.handlePut(item.uuid, e)}
                        handlePatch={(e) => this.handlePatch(item.uuid, e)}
                        handleDel={() => this.handleDel(item.uuid)}>
                    </TodoItem>
                </li>
            );

            return (
                <div>
                    <ul>{listItems}</ul>
                    <SubmitForm handleAdd={this.handleAdd} />
                </div>
            );
        }
    }
}

export default TodoList;
