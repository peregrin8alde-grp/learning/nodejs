import React from 'react';

class TodoItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            task: this.props.todo.task,
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const target = event.target;
        const value = target.name === 'isDone' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <form onSubmit={this.props.handlePut}>
                <input name="isDone" type="checkbox" checked={this.props.todo.isDone} onChange={this.props.handlePatch} />
                <label>{this.props.todo.uuid}:</label>
                <input name="task" type="text" value={this.state.task} onChange={this.handleChange} />
                <button type="submit">Put</button>
                <button type="button" onClick={this.props.handleDel}>Delete</button>
            </form>
        );
    }
}

export default TodoItem;

