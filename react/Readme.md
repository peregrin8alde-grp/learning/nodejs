# React

https://ja.reactjs.org/

## Create React App

https://ja.reactjs.org/docs/create-a-new-react-app.html#create-react-app

### 新規作成

```
docker run \
  --rm \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node npx create-react-app my-app


docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app myapp-typescript --template typescript
```

### 動作確認

```
cd my-app

### バックグラウンド実行したい場合は別途対処がいる
docker run \
  --rm \
  -it \
  --name my-app \
  -p 3000:3000 \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node npm start
```

### ビルド

```
cd my-app

docker run \
  --rm \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node npm run build
```

デフォルトでは js/css の参照が絶対パスとなるため、相対パスにしたい場合は `package.json` で
`"homepage": "./"` といった指定を行う。

## Know How

### proxy

同一ドメインじゃないと CORS が発生する。開発時のみ別ドメインなどの場合は proxy 機能を利用

複数 API サーバを利用する場合に備えて、`Configuring the Proxy Manually` の設定を実施。

https://create-react-app.dev/docs/proxying-api-requests-in-development

```
npm install http-proxy-middleware --save
```

Docker コンテナの場合などは以下のように API サーバに接続できるようにネットワークを
繋ぐことを忘れないこと。

```
docker run \
  --rm \
  -it \
  --name my-app \
  -p 3000:3000 \
  -e "NODE_ENV=production" \
  -u "node" \
  -m "300M" --memory-swap "1G" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  --link nodered \
  node npm start
```

### バケツリレー（prop drilling）問題

親から孫などへと prop を受け渡していくと、中間で不要なデータ所持が生じることがある。

大量のコンポーネントから利用されるのであれば、コンテキストを使ってグローバルなプロパティとして扱う。

https://ja.reactjs.org/docs/context.html

少量であれば、コンポジションを利用する。

https://ja.reactjs.org/docs/composition-vs-inheritance.html
