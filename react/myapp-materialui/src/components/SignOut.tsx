import React from "react";
import IconButton from "@material-ui/core/IconButton";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

type Props = {
  onClick(event: React.MouseEvent<HTMLButtonElement>): void;
};

function SignOut(props: Props) {
  return (
    <IconButton color="inherit" onClick={props.onClick}>
      <AccountCircleIcon />
    </IconButton>
  );
}

export default SignOut;
