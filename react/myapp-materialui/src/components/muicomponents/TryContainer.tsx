import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Container from "@material-ui/core/Container";

const useStyles = makeStyles({
  root: {
    background: "green",
    height: "100vh",
  },
});

function TryContainer() {
  const classes = useStyles();

  return <Container maxWidth="xs" className={classes.root} />;
}

export default TryContainer;
