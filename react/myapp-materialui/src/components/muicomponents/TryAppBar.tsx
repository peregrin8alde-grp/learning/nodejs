import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  menuButton: {},
  title: {
    flexGrow: 1,
  },
});

type PropsSignInButton = {
  onClick(event: React.MouseEvent<HTMLButtonElement>): void;
};

function SignInButton(props: PropsSignInButton) {
  return (
    <Button color="inherit" onClick={props.onClick}>
      Sign In
    </Button>
  );
}

type PropsAccountMenuButton = {
  onClick(event: React.MouseEvent<HTMLLIElement, MouseEvent>): void;
};

function AccountMenuButton(props: PropsAccountMenuButton) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    console.log("handleProfileMenuOpen");
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    console.log("handleMenuClose");
    setAnchorEl(null);
  };

  // Menu を IconButton 内に配置すると onClick が被るので注意
  return (
    <div>
      <IconButton
        aria-label="account of current user"
        onClick={handleProfileMenuOpen}
        color="inherit"
      >
        <AccountCircle />
      </IconButton>
      <Menu
        id="account-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={props.onClick}>Sign Out</MenuItem>
      </Menu>
    </div>
  );
}

function TryAppBar() {
  const classes = useStyles();
  const [isSignedIn, setSignedIn] = React.useState(false);

  const signIn = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();

    console.log("signIn");
    setSignedIn(true);
  };

  const signOut = (
    event: React.MouseEvent<HTMLLIElement, MouseEvent>
  ): void => {
    event.preventDefault();

    console.log("signOut");
    setSignedIn(false);
  };

  const acountButton = isSignedIn ? (
    <AccountMenuButton onClick={(e) => signOut(e)} />
  ) : (
    <SignInButton onClick={(e) => signIn(e)} />
  );

  console.log("TryAppBar");

  return (
    <div>
      <AppBar className={classes.root}>
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6">
            Title
          </Typography>
          {acountButton}
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default TryAppBar;
