import React from "react";
import Dashboard from "./components/dashboard/Dashboard";
import SignIn from "./components/SignIn";
import SignOut from "./components/SignOut";

function App() {
  const [isSignedIn, setSignedIn] = React.useState(false);

  const signIn = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();

    console.log("signIn");
    setSignedIn(true);
  };

  const signOut = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.preventDefault();

    console.log("signOut");
    setSignedIn(false);
  };

  if (isSignedIn) {
    return (
      <Dashboard>
        <SignOut onClick={(e) => signOut(e)}></SignOut>
      </Dashboard>
    );
  }
  return <SignIn onSubmit={(e) => signIn(e)} />;
}

export default App;
