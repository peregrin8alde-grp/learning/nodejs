# Material-ui

https://material-ui.com/ja/

## install

https://material-ui.com/ja/getting-started/installation/

```
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npx create-react-app myapp-materialui --template typescript

cd myapp-materialui

# git でcloneして node_modules がない場合
docker run \
  --rm \
  -it \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npm install

docker run \
  --rm \
  -it \
  --name myapp-typescript \
  -p 3000:3000 \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    yarn start
```

```
yarn add @material-ui/core
```

### Roboto Font

Roboto Font 利用を想定して作られているため、必須。
（`npm` ではなく `yarn` を使うこと）

- https://material-ui.com/ja/components/typography/#general
- https://github.com/DecliningLotus/fontsource/blob/master/packages/roboto/README.md

```
yarn add fontsource-roboto
```

```
import 'fontsource-roboto';
```

### Icons

https://material-ui.com/ja/components/icons/#font-icons

```
yarn add @material-ui/icons

import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import ThreeDRotation from '@material-ui/icons/ThreeDRotation';
```

## template

https://material-ui.com/ja/getting-started/templates/

- `dashboard` の `Chart.js` はチャート用に外部ライブラリを使用しているためここでは外す。

```
mkdir src/components

curl -o src/components/SignIn.js \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/sign-in/SignIn.js


curl -o src/components/dashboard/Dashboard.js \
  --create-dirs \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/dashboard/Dashboard.js
curl -o src/components/dashboard/Deposits.js \
  --create-dirs \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/dashboard/Deposits.js
curl -o src/components/dashboard/Orders.js \
  --create-dirs \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/dashboard/Orders.js
curl -o src/components/dashboard/Title.js \
  --create-dirs \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/dashboard/Title.js
curl -o src/components/dashboard/listItems.js \
  --create-dirs \
  https://raw.githubusercontent.com/mui-org/material-ui/master/docs/src/pages/getting-started/templates/dashboard/listItems.js
```

## Trouble Shooting

- 以下の警告が発生

  ```
  findDOMNode is deprecated in StrictMode
  ```

  - Material-ui の問題らしい？
