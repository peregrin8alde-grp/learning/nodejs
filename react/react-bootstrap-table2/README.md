# react-bootstrap-table2

https://react-bootstrap-table.github.io/react-bootstrap-table2/


## install

```
docker run \
  --rm \
  -e "NODE_ENV=production" \
  -u "node" \
  -v $(pwd):/home/node/app \
  -w "/home/node/app" \
  node \
    npm install react-bootstrap-table-next --save
```

## filter

```
npm install react-bootstrap-table2-filter --save
```
