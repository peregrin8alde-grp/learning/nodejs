import React from 'react';
import './App.css';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';

import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import filterFactory, { textFilter, multiSelectFilter, numberFilter } from 'react-bootstrap-table2-filter';


function App() {

  const products = [
    {
      id: 'aa',
      name: 'bb',
      quality: 0,
      price: 100,
    },
    {
      id: 'bb',
      name: 'cc',
      price: 150,
    },
  ];

  const selectOptions = {
    0: 'good',
    1: 'Bad',
    2: 'unknown'
  };

  const columns = [
    {
      dataField: 'id',
      text: 'Product ID',
      sort: true,
      filter: textFilter(),
    },
    {
      dataField: 'name',
      text: 'Product Name',
      sort: true,
      filter: textFilter(),
    },
    {
      dataField: 'quality',
      text: 'Product Quailty',
      formatter: cell => selectOptions[cell],
      filter: multiSelectFilter({
        options: selectOptions
      })
    },
    {
      dataField: 'price',
      text: 'Product Price',
      sort: true,
      filter: numberFilter()
    }
  ];

  return (
    <BootstrapTable
      keyField='id'
      data={products}
      columns={columns}
      bootstrap4='true'
      filter={ filterFactory() }
    />
  );
}

export default App;
